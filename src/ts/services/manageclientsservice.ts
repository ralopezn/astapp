/**
 * Created by Ricardo on 03/04/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappServices {

    export interface IManageClientsService {
        getClient: (idIssue: number, idClient: number) => ng.IPromise<Entities.Client>;
    }

    class ManageClientsService implements IManageClientsService {

        urlPostRestApi: string;
        urlGetRestApi: string;

        static $inject = ['$http', '$q', 'API_SERVICES_REST', 'astapp.services.ManageIssues'];
        constructor(private $http: ng.IHttpService,
                    private $q: ng.IQService,
                    API_SERVICES_REST: IUrlMethodsRestApi,
                    private manageIssues: IManageIssuesService) {
            this.urlPostRestApi = API_SERVICES_REST.POST_ISSUES_URL;
            this.urlGetRestApi = API_SERVICES_REST.GET_ISSUES_URL;
        }

        getClient(idIssue: number, idClient: number): ng.IPromise<Entities.Client> {
            var deferred = this.$q.defer<Entities.Client>();

            this.manageIssues.getIssue(idIssue).then((issue: Entities.Issue) => {

                if (issue.client.id === idClient) {
                    deferred.resolve(issue.client);
                } else {
                    deferred.reject(null);
                }
            });

            return deferred.promise;
        }
    }

    angular.module('astapp.services')
        .service('astapp.services.ManageClients', ManageClientsService);
}