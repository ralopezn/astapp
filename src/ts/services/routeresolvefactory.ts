/**
 * Created by ralopezn on 31/03/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappServices {
    'use strict';

    export interface IRouteResolveFactory {
        issuesView: IResolveIssuesView;
        issueDetailView: IResolveIssueDetailView;
        userView: IResolveUserView;
        clientView: IResolveClientView;
        geoLocationView: IResolveGeoLocationView;
        dashboard: IResolveDashBoardView;
    }

    interface IResolveIssuesView {
        getIssues: [string, {(manageIssues: IManageIssuesService): any}];
    }

    interface IResolveIssueDetailView {
        getIssue: [string, string, {($stateParams: IParametersQueryString, manageIssues: IManageIssuesService): any}];
    }

    interface IResolveUserView {
        getUserResolveIssue: {(): Entities.User};
    }

    interface IResolveClientView {
        getClientFromIssue: [string, string, {($stateParams: IParametersQueryString, manageClients: IManageClientsService): any}];
    }

    interface IResolveGeoLocationView {
        getCurrentLocation: [string, {(geoLocation: IGeoLocationService): any}];
    }

    export interface IParametersQueryString extends angular.ui.IStateParamsService {
        idIssueDetailSelected: number;
        idClientFromIssue: number;
        issuesFiltered: string;
    }

    interface IResolveDashBoardView {
        statusOpenMoreThanOneDay: [string, {(manageIssues: IManageIssuesService): any}];
        statusPendingMoreThanOneDay: [string, {(manageIssues: IManageIssuesService): any}];
        statusOpenCurrentDay: [string, {(manageIssues: IManageIssuesService): any}];
        statusPendingCurrentDay: [string, {(manageIssues: IManageIssuesService): any}];
    }

    function RouteResolveFactory() : IRouteResolveFactory {



        var self: IRouteResolveFactory = {
            issuesView: {
                getIssues: ['astapp.services.ManageIssues', (manageIssues: IManageIssuesService) => {
                    return manageIssues.getIssues().then((issues: Array<Entities.Issue>) => { return issues; });
                }]
            },
            issueDetailView: {
                getIssue: ['$stateParams', 'astapp.services.ManageIssues', ($stateParams: IParametersQueryString, manageIssues: IManageIssuesService) => {
                    var idIssueSelected: number = $stateParams.idIssueDetailSelected;
                    return manageIssues.getIssue(idIssueSelected).then((issue: Entities.Issue) => { return issue; });
                }]
            },
            userView: {
                getUserResolveIssue: ():Entities.User => {

                    var location: Entities.Location = <Entities.Location>{
                        latitude: 8.65,
                        longitude: -27.9531204
                    };
                    var user: Entities.User = <Entities.User>{
                        userName: 'root',
                        password: 'root',
                        location: location
                    };

                    return user;
                }
            },
            clientView: {
                getClientFromIssue: ['$stateParams', 'astapp.services.ManageClients', ($stateParams: IParametersQueryString, manageClients: IManageClientsService) => {
                    var idIssueSelected: number = $stateParams.idIssueDetailSelected;
                    var idClientSelected: number = $stateParams.idClientFromIssue;
                    return manageClients.getClient(idIssueSelected, idClientSelected).then((client: Entities.Client) => {
                        return client;
                    });
                }]
            },
            geoLocationView: {
                getCurrentLocation: ['astapp.services.GeoLocation', (geoLocation: IGeoLocationService) => {
                    return geoLocation.getCurrentPosition().then((location: Entities.Location) => { return location; });
                }]
            },
            dashboard: {
                statusOpenMoreThanOneDay: ['astapp.services.ManageIssues', (manageIssues: IManageIssuesService) => {
                    return manageIssues.getIssuesWithoutAttentionMoreThanOneDay(Entities.StatusOptions.Open)
                        .then((alert: Entities.DashBoardAlert) => { return alert; });
                }],
                statusPendingMoreThanOneDay: ['astapp.services.ManageIssues', (manageIssues: IManageIssuesService) => {
                    return manageIssues.getIssuesWithoutAttentionMoreThanOneDay(Entities.StatusOptions.Pending)
                        .then((alert: Entities.DashBoardAlert) => { return alert; });
                }],
                statusOpenCurrentDay: ['astapp.services.ManageIssues', (manageIssues: IManageIssuesService) => {
                    return manageIssues.getIssuesWithoutAttentionCurrentDay(Entities.StatusOptions.Open)
                        .then((alert: Entities.DashBoardAlert) => { return alert; });
                }],
                statusPendingCurrentDay: ['astapp.services.ManageIssues', (manageIssues: IManageIssuesService) => {
                    return manageIssues.getIssuesWithoutAttentionCurrentDay(Entities.StatusOptions.Pending)
                        .then((alert: Entities.DashBoardAlert) => { return alert; });
                }]
            }
        };

        return self;
    }

    angular.module('astapp.services')
        .constant('astapp.services.RouteResolve', RouteResolveFactory());

}