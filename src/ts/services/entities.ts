/**
 * Created by ralopezn on 31/03/2015.
 */

module Entities {

    export enum StatusOptions {
        Open = 0,
        Close = 1,
        Pending = 2
    }

    export interface Location {
        latitude: number;
        longitude: number;
    }

    export interface User {
        userName: string;
        password: string;
        location: Location;
    }

    export interface Issue {
        id: number;
        status: StatusOptions;
        client: Client;
        assignee: User;
        assignationDate: Date;
        creationDate: Date;
        dateLastModification: Date;
        info: string;
        comments: string;
    }

    export interface Client {
        id: number;
        name: string;
        address: string;
        location: Location;
    }

    export interface DashBoardAlert {
        id: number;
        count: number;
        referIssuesId: string;
    }
}