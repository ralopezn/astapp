/**
 * Created by Ricardo on 02/04/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappServices {
    'use strict';

    export interface IGeoLocationService {
        getCurrentPosition: {(): ng.IPromise<Entities.Location>};
    }

    export interface IGeoLocationReturnPromise<T> {
        data: T;
    }

    class GeoLocationService {

        static $inject = ['$q', '$cordovaGeolocation', '$ionicPlatform'];
        constructor(private $q: ng.IQService, private $cordovaGeolocation, private $ionicPlatform) {

        }

        getCurrentPosition(): ng.IPromise<Entities.Location> {
            var self = this;
            var posOptions = { timeout: 15000, enableHighAccuracy: true };
            var deferred = this.$q.defer<Entities.Location>();
            var location: Entities.Location = <Entities.Location>{};

            this.$ionicPlatform.ready(function() {
                self.$cordovaGeolocation.getCurrentPosition(posOptions)
                    .then(function (position) {
                        location.latitude = position.coords.latitude;
                        location.longitude = position.coords.longitude;

                        deferred.resolve(location);
                    }, function(err) {
                        deferred.reject(err);
                    });
            });

            return deferred.promise;
        }
    }

    angular.module('astapp.services')
        .service('astapp.services.GeoLocation', GeoLocationService);
}