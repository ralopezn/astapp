/**
 * Created by Ricardo on 03/04/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappServices {

    interface IRestApiSettingsConstant {
        url: string;
    }

    interface IPathRestApiSettings extends IRestApiSettingsConstant {
        pathIssues: string;
        pathClients: string;
        pathUsers: string;
    }

    export interface IUrlMethodsRestApi {
        POST_ISSUES_URL: string;
        GET_ISSUES_URL: string;
        POST_CLIENTS_URL: string;
        GET_CLIENTS_URL: string;
        POST_USERS_URL: string;
        GET_USERS_URL: string;
    }

    function settingsConstant(): IUrlMethodsRestApi {

        var sourceApi = {
            ip: 'localhost',
            port: 3000
        };

        var paramsRest: IPathRestApiSettings = {
            url: 'http://' + sourceApi.ip + ':' + sourceApi.port + '/api',
            pathIssues: '/Issues/',
            pathClients: '/Clients/',
            pathUsers: '/Users/'
        };

        var self: IUrlMethodsRestApi = {
            POST_ISSUES_URL: paramsRest.url + paramsRest.pathIssues,
            GET_ISSUES_URL: paramsRest.url + paramsRest.pathIssues,
            POST_CLIENTS_URL: paramsRest.url + paramsRest.pathClients,
            GET_CLIENTS_URL: paramsRest.url + paramsRest.pathClients,
            POST_USERS_URL: paramsRest.url + paramsRest.pathUsers,
            GET_USERS_URL: paramsRest.url + paramsRest.pathUsers
        };

        return self;
    }

    angular.module('astapp.services')
        .constant('API_SERVICES_REST', settingsConstant());
}