/**
 * Created by Ricardo on 03/04/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappServices {

    export interface IManageIssuesService {
        createIssue: (issue: Entities.Issue) => void;
        getIssues: () => ng.IPromise<Array<Entities.Issue>>;
        getIssue: (id: number) => ng.IPromise<Entities.Issue>;
        getIssuesWithoutAttentionMoreThanOneDay: (status: Entities.StatusOptions) => ng.IPromise<Entities.DashBoardAlert>;
        getIssuesWithoutAttentionCurrentDay: (status: Entities.StatusOptions) => ng.IPromise<Entities.DashBoardAlert>;
    }


    var location: Entities.Location = <Entities.Location>{
        latitude: 4.808806,
        longitude: -75.694623
    };
    var user: Entities.User = <Entities.User>{
        userName: 'root',
        password: 'root',
        location: location
    };
    var client: Entities.Client = <Entities.Client>{
        id: 1,
        name: 'NombreCliente1',
        address: 'DireccionCliente1',
        location: location
    };
    var issueOpen1: Entities.Issue = <Entities.Issue>{
        id: 1,
        status: Entities.StatusOptions.Open,
        client: client,
        assignee: user,
        assignationDate: new Date('02/25/2015 07:45'),
        creationDate: new Date('10/09/2014 08:45'),
        info: 'Info issue open 1'
    };
    var issueOpen2: Entities.Issue = <Entities.Issue>{
        id: 2,
        status: Entities.StatusOptions.Open,
        client: client,
        assignee: user,
        assignationDate: new Date('03/15/2015 15:13'),
        creationDate: new Date('10/09/2014 09:45'),
        info: 'Info issue open 2'
    };
    var issuePending1: Entities.Issue = <Entities.Issue>{
        id: 3,
        status: Entities.StatusOptions.Pending,
        client: client,
        assignee: user,
        assignationDate: new Date('01/03/2014 19:01'),
        creationDate: new Date('10/05/2013 08:45'),
        info: 'Info issue pending 1'
    };
    var issuePending2: Entities.Issue = <Entities.Issue>{
        id: 4,
        status: Entities.StatusOptions.Pending,
        client: client,
        assignee: user,
        assignationDate: new Date('03/01/2015 19:01'),
        creationDate: new Date('10/05/2013 08:45'),
        info: 'Info issue pending 2'
    };
    var issueClose1: Entities.Issue = <Entities.Issue>{
        id: 5,
        status: Entities.StatusOptions.Open,
        client: client,
        assignee: user,
        assignationDate: new Date('04/06/2015 07:48'), //new Date('04/01/2015 07:48'),
        creationDate: new Date('10/09/2014 08:45'),
        info: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.<br><br>' +
        'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.<br><br>' +
        'Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate '
    };


    class ManageIssuesService implements IManageIssuesService {

        urlPostRestApi: string;
        urlGetRestApi: string;

        dashBoardAlert: Entities.DashBoardAlert;

        static $inject = ['$http', '$q', 'API_SERVICES_REST'];
        constructor(private $http: ng.IHttpService,
                    private $q: ng.IQService,
                    API_SERVICES_REST: IUrlMethodsRestApi) {
            this.urlPostRestApi = API_SERVICES_REST.POST_ISSUES_URL;
            this.urlGetRestApi = API_SERVICES_REST.GET_ISSUES_URL;
        }

        createIssue(issue: Entities.Issue): void {
            var i = 0;
        }

        getIssues(): ng.IPromise<Array<Entities.Issue>> {
            var deferred = this.$q.defer<Array<Entities.Issue>>();

            deferred.resolve([
                issueOpen1,
                issueOpen2,
                issuePending1,
                issuePending2,
                issueClose1
            ]);

            return deferred.promise;
        }

        getIssue(id: number): ng.IPromise<Entities.Issue> {
            var deferred = this.$q.defer<Entities.Issue>();
            var issueFound: Entities.Issue = null;

            this.getIssues().then((issues: Array<Entities.Issue>) => {

                for (var index = 0; index < issues.length; ++index) {
                    issueFound = issues[index];
                    if (issueFound.id === id) {
                        break;
                    } else {
                        issueFound = null;
                    }
                }

                deferred.resolve(issueFound);
            });

            return deferred.promise;
        }

        getIssuesWithoutAttentionMoreThanOneDay(status: Entities.StatusOptions) {
            var diferred = this.$q.defer();
            var self = this;
            var issue:Entities.Issue = null;
            var count:number = 0;
            var alert = <Entities.DashBoardAlert>{};
            var issuesId: string = '';

            this.getIssues().then((issues: Array<Entities.Issue>) => {

                for (var index = 0; index < issues.length; ++index) {
                    issue = issues[index];
                    if (issue.status === status) {
                        var currentDate:Date = new Date();
                        var diff: number = 0;

                        if (issue.assignationDate !== null) {
                            diff = self.diffFechas(issue.assignationDate.getTime(), currentDate.getTime());
                        }

                        if (diff !== 0 && diff > 24) {
                            ++count;
                            issuesId += issue.id + ',';
                        }
                    }
                }

                alert.count = count;
                alert.referIssuesId = issuesId;

                diferred.resolve(alert);
            });

            return diferred.promise;
        }

        diffFechas(fecha1: number, fecha2: number): number {
            // el retono da en Horas.
            return (fecha2 - fecha1) / 1000 / 60 / 60;
        }

        getIssuesWithoutAttentionCurrentDay(status: Entities.StatusOptions) {
            var diferred = this.$q.defer();
            var self = this;
            var issue:Entities.Issue = null;
            var count:number = 0;
            var alert = <Entities.DashBoardAlert>{};
            var issuesId: string = '';

            this.getIssues().then((issues: Array<Entities.Issue>) => {

                for (var index = 0; index < issues.length; ++index) {
                    issue = issues[index];
                    if (issue.status === status) {
                        var currentDate:Date = new Date();
                        var diff: number = 0;

                        if (issue.assignationDate !== null) {
                            diff = self.diffFechas(issue.assignationDate.getTime(), currentDate.getTime());
                        }

                        if (diff !== 0 && diff <= 24) {
                            ++count;
                            issuesId += issue.id + ',';
                        }
                    }
                }

                alert.count = count;
                alert.referIssuesId = issuesId;

                diferred.resolve(alert);
            });

            return diferred.promise;
        }
    }

    angular.module('astapp.services')
        .service('astapp.services.ManageIssues', ManageIssuesService);
}