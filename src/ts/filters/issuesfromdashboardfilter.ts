/**
 * Created by ralopezn on 06/04/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappFilters {

    function IssuesFromDashboardFilter($stateParams: astappServices.IParametersQueryString) {
        return (issues: Array<Entities.Issue>) => {
            issues = issues || [];

            var issuesFiltered:Array<string> = [];
            var issuesOutFiltered: Array<Entities.Issue> = [];
            var issuesFilteredParam: string = $stateParams.issuesFiltered || '0';

            if (issuesFilteredParam !== '0') {
                issuesFiltered = issuesFilteredParam.split(',');
            }

            if (issuesFiltered.length > 0) {
                for (var index = 0; index < issues.length; ++index) {
                    var issue: Entities.Issue = issues[index];
                    if (issuesFiltered.indexOf(issue.id.toString()) !== -1) {
                        issuesOutFiltered.push(issue);
                    }
                }

                return issuesOutFiltered;
            } else {
                return issues;
            }
        };
    }

    angular.module('astapp.filters')
        .filter('issuesFromDashboard', ['$stateParams', IssuesFromDashboardFilter]);
}