/**
 * Created by ralopezn on 31/03/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappControllers {
    'use strict';

    interface IssueMapControllerScope extends ng.IScope {
        locationFromUser: Entities.Location;
        locationToClient: Entities.Location;
        map: {};
        markers: Array<{}>;
    }

    class IssueMapController {

        //static $inject = ['$scope', 'getUserResolveIssue', 'getClientFromIssue', 'getCurrentLocation'];
        static $inject = ['$scope', 'getCurrentLocation', 'getUserResolveIssue', 'getClientFromIssue'];
        constructor($scope: IssueMapControllerScope,
                    getCurrentLocation: Entities.Location,
                    user: Entities.User,
                    client: Entities.Client) {
                    //user: Entities.User,
                    //client: Entities.Client,
                    //getCurrentLocation: Entities.Location) {
            //Controller Body

            $scope.locationToClient = client.location;
            $scope.locationFromUser = user.location;

            user.location = angular.copy(getCurrentLocation);

            $scope.map = {
                center: user.location, //{ latitude: 4.806666, longitude: -75.721695},
                zoom: 15,
                options: {
                    streetViewControl: false,
                    //zoomControl: false,
                    scaleControl: true,
                    mapTypeControl: false,
                    minZoom: 12,
                    maxZoom: 16,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.TOP_LEFT
                    }
                }
            };

            $scope.markers = [
                {
                    id: 1,
                    coords: getCurrentLocation,
                    options: {
                        animation: google.maps.Animation.DROP,
                        draggable: false
                    },
                    icon: 'img/map-marker-user-male-64.png'
                },
                {
                    id: 2,
                    coords: client.location,
                    options: {
                        animation: google.maps.Animation.DROP,
                        draggable: false
                    },
                    icon: 'img/map-marker-client-64.png'
                }
            ];
        }
    }

    angular.module('astapp.controllers')
        .controller('astapp.controllers.IssueMapController', IssueMapController);
}

//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/issueMap',
//                        templateUrl: 'templates/issueMap-template.html',
//                        controller: 'astapp.controllers.IssueMapController'
//                    });

//For use inside template:
//    {{data}}

//Check dependencies inside app.ts
//    angular.module('astapp.controllers', []);
//    angular.module('app', ['ionic', 'astapp.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/issueMapcontroller.js"></script>