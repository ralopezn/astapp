/**
 * Created by ralopezn on 31/03/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappControllers {
    'use strict';

    interface IssueClientControllerScope extends ng.IScope {
        client: Entities.Client;
    }

    class IssueClientController {

        static $inject = ['$scope', 'getClientFromIssue'];
        constructor($scope:IssueClientControllerScope, client: Entities.Client) {
            //Controller Body
            $scope.client = client;
        }
    }

    angular.module('astapp.controllers')
        .controller('astapp.controllers.IssueClientController', IssueClientController);
}

//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/issueClient',
//                        templateUrl: 'templates/issueClient-template.html',
//                        controller: 'astapp.controllers.IssueClientController'
//                    });

//For use inside template:
//    {{data}}

//Check dependencies inside app.ts
//    angular.module('astapp.controllers', []);
//    angular.module('app', ['ionic', 'astapp.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/issueClientcontroller.js"></script>