/**
 * Created by ralopezn on 31/03/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappControllers {
    'use strict';

    interface IssueDetailControllerScope extends ng.IScope {
        issue: Entities.Issue;
        status: {};
    }

    class IssueDetailController {

        static $inject = ['$scope', 'getIssue'];
        constructor($scope:IssueDetailControllerScope, issue: Entities.Issue) {
            //Controller Body
            $scope.issue = issue;
            $scope.status = Entities.StatusOptions;
        }
    }

    angular.module('astapp.controllers')
        .controller('astapp.controllers.IssueDetailController', IssueDetailController);
}

//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/issueDetail',
//                        templateUrl: 'templates/issueDetail-template.html',
//                        controller: 'astapp.controllers.IssueDetailController'
//                    });

//For use inside template:
//    {{data}}

//Check dependencies inside app.ts
//    angular.module('astapp.controllers', []);
//    angular.module('app', ['ionic', 'astapp.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/issueDetailcontroller.js"></script>