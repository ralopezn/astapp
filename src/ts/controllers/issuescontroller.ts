/**
 * Created by ralopezn on 31/03/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappControllers {
    'use strict';

    interface IssuesControllerScope extends ng.IScope {
        issues: Array<Entities.Issue>;
    }

    class IssuesController {

        static $inject = ['$scope', 'getIssues'];
        constructor($scope: IssuesControllerScope, issues: Array<Entities.Issue>) {
            //Controller Body
            $scope.issues = issues;
        }
    }

    angular.module('astapp.controllers')
        .controller('astapp.controllers.IssuesController', IssuesController);
}

//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/issues',
//                        templateUrl: 'templates/issues-template.html',
//                        controller: 'astapp.controllers.IssuesController'
//                    });

//For use inside template:
//    {{data}}

//Check dependencies inside app.ts
//    angular.module('astapp.controllers', []);
//    angular.module('app', ['ionic', 'astapp.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/issuescontroller.js"></script>