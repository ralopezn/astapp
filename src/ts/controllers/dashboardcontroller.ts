/**
 * Created by ralopezn on 06/04/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappControllers {
    'use strict';

    interface IDashBoardControllerScope extends ng.IScope {
        alertWithStatusOpen: Entities.DashBoardAlert;
        alertWithStatusPending: Entities.DashBoardAlert;
        alertCurrentDayWithStatusOpen: Entities.DashBoardAlert;
        alertCurrentDayWidhStatusPending: Entities.DashBoardAlert;
    }

    class DashBoardController {

        static $inject = ['$scope', 'statusOpenMoreThanOneDay',
                                    'statusPendingMoreThanOneDay',
                                    'statusOpenCurrentDay',
                                    'statusPendingCurrentDay'];
        constructor($scope: IDashBoardControllerScope,
                    statusOpenMoreThanOneDay: Entities.DashBoardAlert,
                    statusPendingMoreThanOneDay: Entities.DashBoardAlert,
                    statusOpenCurrentDay: Entities.DashBoardAlert,
                    statusPendingCurrentDay: Entities.DashBoardAlert) {
            //Controller Body
            $scope.alertWithStatusOpen = statusOpenMoreThanOneDay;
            $scope.alertWithStatusPending = statusPendingMoreThanOneDay;
            $scope.alertCurrentDayWithStatusOpen = statusOpenCurrentDay;
            $scope.alertCurrentDayWidhStatusPending = statusPendingCurrentDay;
        }
    }

    angular.module('astapp.controllers')
        .controller('astapp.controllers.DashBoardController', DashBoardController);
}

//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/iDashBoard',
//                        templateUrl: 'templates/iDashBoard-template.html',
//                        controller: 'astapp.controllers.IDashBoardController'
//                    });

//For use inside template:
//    {{data}}

//Check dependencies inside app.ts
//    angular.module('astapp.controllers', []);
//    angular.module('app', ['ionic', 'astapp.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/iDashBoardcontroller.js"></script>