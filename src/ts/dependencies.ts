/**
 * Created by ralopezn on 31/03/2015.
 */
///<reference path="../../tools/typings/tsd.d.ts" />
///<reference path="../../tools/typings/typescriptApp.d.ts" />

((): void => {

    angular.module('astapp.filters', []);
    angular.module('astapp.controllers', []);
    angular.module('astapp.services', []);
    angular.module('astapp.dependencies', [
        'astapp.controllers',
        'astapp.services',
        'astapp.filters'
    ]);

})();