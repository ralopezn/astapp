///<reference path="../../tools/typings/tsd.d.ts" />
///<reference path="../../tools/typings/typescriptApp.d.ts" />

/**
 * Created by ralopezn on 25/03/2015.
 */
((): void => {

    angular.module('astapp', ['ionic', 'astapp.dependencies', 'uiGmapgoogle-maps', 'ngCordova'])

        .run(function($ionicPlatform) {
            $ionicPlatform.ready(function() {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                    // org.apache.cordova.statusbar required
                    //StatusBar.styleDefault();
                }
            });
        })
        .config(['$ionicConfigProvider', ($ionicConfigProvider) => {
            $ionicConfigProvider.backButton.previousTitleText(false).text('');
            $ionicConfigProvider.views.maxCache(0);
            $ionicConfigProvider.tabs.position('bottom');
        }])
        .config((uiGmapGoogleMapApiProvider) => {
            uiGmapGoogleMapApiProvider.configure({
                key: 'AIzaSyBnbLbGlEkm0F8IYGnyv3DCAarqvhyGPXQ',
                v: '3.17',
                libraries: 'geometry,visualization'
            });
        })
        .config(['$stateProvider', '$urlRouterProvider', 'astapp.services.RouteResolve',
            ($stateProvider, $urlRouterProvider, routeResolveConstant: astappServices.IRouteResolveFactory) => {
                $urlRouterProvider.otherwise('/login');
                $stateProvider
                    .state('login', {
                        url: '/login',
                        cache: false,
                        templateUrl: 'templates/login-template.html'
                    })
                    .state('main', {
                        url: '/main',
                        cache: false,
                        abstract: true,
                        templateUrl: 'templates/main-tabs-template.html'
                    })
                    .state('main.dash', {
                        url: '/dash',
                        cache: false,
                        resolve: routeResolveConstant.dashboard,
                        views: {
                            'main-dashboard': {
                                templateUrl: 'templates/dashboard-template.html',
                                controller: 'astapp.controllers.DashBoardController'
                            }
                        }
                    })
                    .state('main.issues', {
                        url: '/issues-{issuesFiltered:string}',
                        cache: false,
                        resolve: routeResolveConstant.issuesView,
                        views: {
                            'main-issues': {
                                templateUrl: 'templates/issues-template.html',
                                controller: 'astapp.controllers.IssuesController'
                            }
                        }
                    })
                    .state('main.issue-detail', {
                        url: '/issues/{idIssueDetailSelected:int}',
                        cache: false,
                        resolve: routeResolveConstant.issueDetailView,
                        views: {
                            'main-issues': {
                                templateUrl: 'templates/issue-detail-template.html',
                                controller: 'astapp.controllers.IssueDetailController'
                            }
                        }
                    })
                    .state('main.issue-client', {
                        url: '/issues/{idIssueDetailSelected:int}/client/{idClientFromIssue:int}',
                        cache: false,
                        resolve: routeResolveConstant.clientView,
                        views: {
                            'main-issues': {
                                templateUrl: 'templates/issue-client-template.html',
                                controller: 'astapp.controllers.IssueClientController'
                            }
                        }
                    })
                    .state('main.issue-map', {
                        url: '/issues-{idIssueDetailSelected:int}-{idClientFromIssue:int}/map',
                        cache: false,
                        resolve: angular.extend({}, routeResolveConstant.geoLocationView, routeResolveConstant.userView, routeResolveConstant.clientView),
                        views: {
                            'main-issues': {
                                templateUrl: 'templates/issue-map-template.html',
                                controller: 'astapp.controllers.IssueMapController'
                            }
                        }
                    })
                ;
        }]);
})();